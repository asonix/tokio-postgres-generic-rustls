# tokio-postgres-generic-rustls
_An impelementation of TLS based on rustls for tokio-postgres_

This crate allows users to select a crypto backend, or bring their own, rather than relying on
primitives provided by `ring` directly. This is done through the use of x509-cert for
certificate parsing rather than X509-certificate, while also adding an abstraction for
computing digests.

By default, tokio-postgres-generic-rustls does not provide a digest implementation, but one or
more are provided behind crate features.

| Feature      | Impelementation    |
| ------------ | ------------------ |
| `aws-lc-rs`  | `AwsLcRsDigest`    |
| `ring`       | `RingDigest`       |
| `rustcrypto` | `RustcryptoDigest` |

## Usage
Using this crate is fairly straightforward. First, select your digest impelementation via crate
features (or provide your own), then construct rustls connector for tokio-postgres with your
rustls client configuration.

The following example demonstrates providing a custom digest backend.

```rust
use tokio_postgres_generic_rustls::{DigestImplementation, DigestAlgorithm, MakeRustlsConnect};

#[derive(Clone)]
struct DemoDigest;

impl DigestImplementation for DemoDigest {
    fn digest(&self, algorithm: DigestAlgorithm, bytes: &[u8]) -> Vec<u8> {
        todo!("digest it")
    }
}

fn main() {
    let cert_store = rustls::RootCertStore::empty();

    let config = rustls::ClientConfig::builder()
        .with_root_certificates(cert_store)
        .with_no_client_auth();

    let tls = MakeRustlsConnect::new(config, DemoDigest);

    let connect_future = tokio_postgres::connect("postgres://username:password@localhost:5432/db", tls);

    // connect_future.await;
}
```

## License
This project is licensed under either of

- Apache License, Version 2.0, (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
- MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option.
